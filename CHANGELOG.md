# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.3.0](https://gitlab.com/unhcs/discord/project-template-tsnode17/compare/v1.2.0...v1.3.0) (2022-05-31)


### Bug Fixes

* package main pointing to / instead of build/ ([ab7dbcd](https://gitlab.com/unhcs/discord/project-template-tsnode17/commit/ab7dbcdc5f02370b4172c6d8f4f82eb2c3ddf512))

## 1.2.0 (2022-03-14)

## 1.1.0 (2022-03-14)

### Features

-   add vscode workspace settings ([ffa64c7](https://gitlab.com/unh-cs-department/project-template-tsnode17/commit/ffa64c7ca235c45531ad48914d8191209e7b9c4a))

### 1.0.0 (2022-03-14)
